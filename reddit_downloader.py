import os
import asyncio
import aiohttp
import aiofiles

async def main():
    subreddit = input('Enter subreddit:\n')
    os.chdir("$HOME")
    if not os.path.exists(subreddit):
        os.makedirs(subreddit)
    before = 't3_cdub3j'
    count = 0
    num = 0
    
    async def llamada_api(subreddit, before, str: count):
        async with aiohttp.request("GET", f'https://www.reddit.com/r/{subreddit}/hot.json?limit=100&after={before}&count={count}', headers={'User-Agent' : "Magic Browser"}) as url:
            data = await url.json()
            print(f'Request to... [ http://www.reddit.com/ ] with subreddit: {subreddit}')
            return data
    
    posts = await llamada_api(subreddit, before, count)
    before = posts['data']['after']
    
    while posts['data']['children'] is not []:
        count += 1
        before = posts['data']['after']
        
        for post in posts['data']['children']:
            pick = post['data']['url']
            filename = f'{subreddit}/{subreddit}-{pick.split("/")[-1]}'
            filename2 = f'{subreddit}-{pick.split("/")[-1]}'
    
            if len(filename2) >= 50:
                print('TOO LONG!')
            else:
                try:
                    if post['data']['post_hint'] =='image':
                        if filename2 not in os.listdir(subreddit):
                            async with aiohttp.request("GET", pick, headers={'User-Agent' : 'Magic Browser'}) as url:
                                if url.status == 200:
                                    mode = 'wb'
                                    file_creation = await aiofiles.open(f'{filename}', mode=mode)
                                    await file_creation.write(await url.read())
                                    await file_creation.close()
                                    print(f'Downloaded {num}: {filename}')
                                    num += 1 
                        else:
                            print('Image duplicated')
                    else:
                        pass
                except KeyError:
                    pass
        print(count)
        posts = await llamada_api(subreddit, before, count)
        await asyncio.sleep(5)
asyncio.run(main())

